using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionController : MonoBehaviour
{
    public BallMovement ballMovement;
    public ScoreController scoreController;

    void BounceFromBracket(Collision2D c) {

        Vector3 ballPosition = this.transform.position;
        Vector3 bracketPosition = c.gameObject.transform.position;

        float bracketHight = c.collider.bounds.size.y;

        float x;
        if (c.gameObject.name == "Player 1") {
            x = 1;
        }
        else {
            x = -1;
        }
        
        float y = (ballPosition.y - bracketPosition.y) / bracketHight;
        this.ballMovement.IncreaseHitCounter();
        this.ballMovement.MoveBall(new Vector2(x, y));
    } 
    
    void OnCollisionEnter2D(Collision2D c) {
        if (c.gameObject.name == "Player 1" || c.gameObject.name == "Player 2") {
            this.BounceFromBracket(c);
        } else if (c.gameObject.name == "WallLeft") {
            Debug.Log("Collision with WallLeft");
            this.scoreController.GoalPlayer2();
            StartCoroutine(this.ballMovement.StartBall(true));

        } else if (c.gameObject.name == "WallRight") {
            Debug.Log("Collision with WallRight");
            this.scoreController.GoalPlayer1();
            StartCoroutine(this.ballMovement.StartBall(false));
        }
} }
